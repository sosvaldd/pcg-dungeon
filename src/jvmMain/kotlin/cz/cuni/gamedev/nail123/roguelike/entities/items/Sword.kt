package cz.cuni.gamedev.nail123.roguelike.entities.items

import cz.cuni.gamedev.nail123.roguelike.entities.Player
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles

class Sword(val attackPower: Int, val critChance: Float, val RNG: Boolean, val prefix: String): Weapon(GameTiles.SWORD) {
    override fun onEquip(character: HasInventory) {
        if (character is Player) {
            character.attack += attackPower
            character.critChance += critChance
            character.RNG = RNG
        }
    }

    override fun onUnequip(character: HasInventory) {
        if (character is Player) {
            character.attack -= attackPower
            character.critChance -= critChance
            character.RNG = false
        }
    }

    override fun toString(): String {
        return prefix + "Sword($attackPower)"
    }
}