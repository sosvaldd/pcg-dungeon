package cz.cuni.gamedev.nail123.roguelike.world.worlds;

import cz.cuni.gamedev.nail123.roguelike.blocks.Floor;
import cz.cuni.gamedev.nail123.roguelike.blocks.Wall;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Snake;
import cz.cuni.gamedev.nail123.roguelike.entities.items.HealthPot;
import cz.cuni.gamedev.nail123.roguelike.entities.objects.Stairs;
import cz.cuni.gamedev.nail123.roguelike.events.LoggedEvent;
import cz.cuni.gamedev.nail123.roguelike.world.Area;
import cz.cuni.gamedev.nail123.roguelike.world.World;
import cz.cuni.gamedev.nail123.roguelike.world.builders.AreaBuilder;
import cz.cuni.gamedev.nail123.roguelike.world.builders.EmptyAreaBuilder;
import org.hexworks.zircon.api.data.Position3D;
import org.hexworks.zircon.api.data.Size3D;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class SampleJavaWorld extends World {
    int currentLevel = 0;
    private static final Random r = new Random();

    public SampleJavaWorld() {
    }

    @NotNull
    @Override
    public Area buildStartingArea() {
        return buildLevel();
    }

    // area includes surrounding walls
    // width and height include surrounding walls
    // x = column, y = row
    // change SIZE_TO_SPLIT and MIN_ROOM_DIM to influence room sizes
    private final int SIZE_TO_SPLIT = 19;
    private final int MIN_ROOM_DIM = 5;
    private void binarySpacePartitioning(AreaBuilder builder, Position3D leftUpper, int width, int height, int depth) {
        // space too small
        if (width < SIZE_TO_SPLIT && height < SIZE_TO_SPLIT)
            return;

//        // for debugging
//        if (depth > 4)
//            return;

        // vertical split
        if (width > height) {
            int shift = r.nextInt(width - (2 * MIN_ROOM_DIM + 2));
            int x = (MIN_ROOM_DIM + 1) + shift + leftUpper.getX();

            // ensure we dont interfere with another door
            while (builder.getBlocks().get(Position3D.create(x, leftUpper.getY(), 0)).getClass() == Floor.class) {
                int tmp = r.nextInt(2);
                int move = tmp == 0 ? 1 : -1;
                x += move;
            }
            while (builder.getBlocks().get(Position3D.create(x, leftUpper.getY() + (height - 1), 0)).getClass() == Floor.class) {
                int tmp = r.nextInt(2);
                int move = tmp == 0 ? 1 : -1;
                x += move;
            }

            // build wall
            for (int i = 1; i < height - 1; i++) {
                builder.getBlocks().put(Position3D.create(x, leftUpper.getY() + i, 0), new Wall());
            }

            // build door
            int doorRange = height - 4; // 2 for walls, 2 for tiles next to walls
            int doorY = 2 + r.nextInt(doorRange);
            builder.getBlocks().put(Position3D.create(x, leftUpper.getY() + doorY, 0), new Floor());
            int tmp = r.nextInt(2);
            int move = tmp == 0 ? 1 : -1;
            doorY += move;
            builder.getBlocks().put(Position3D.create(x, leftUpper.getY() + doorY, 0), new Floor());

            // call recursively
            // left
            binarySpacePartitioning(builder, leftUpper, (x - leftUpper.getX()) + 1, height, depth + 1);
            // right
            binarySpacePartitioning(builder, Position3D.create(x, leftUpper.getY(), 0), ((leftUpper.getX() + (width - 1)) - x) + 1, height, depth + 1);
        }

        // horizontal split
        else {
            int shift = r.nextInt(height - (2 * MIN_ROOM_DIM + 2));
            int y = (MIN_ROOM_DIM + 1) + shift + leftUpper.getY();

            // ensure we dont interfere with another door
            while (builder.getBlocks().get(Position3D.create(leftUpper.getX(), y, 0)).getClass() == Floor.class) {
                int tmp = r.nextInt(2);
                int move = tmp == 0 ? 1 : -1;
                y += move;
            }
            while (builder.getBlocks().get(Position3D.create(leftUpper.getX() + (width - 1), y, 0)).getClass() == Floor.class) {
                int tmp = r.nextInt(2);
                int move = tmp == 0 ? 1 : -1;
                y += move;
            }

            // build wall
            for (int i = 1; i < width - 1; i++) {
                builder.getBlocks().put(Position3D.create(leftUpper.getX() + i, y, 0), new Wall());
            }

            // build door
            int doorRange = width - 4; // 2 for walls, 2 for tiles next to walls
            int doorX = 2 + r.nextInt(doorRange);
            builder.getBlocks().put(Position3D.create(leftUpper.getX() + doorX, y, 0), new Floor());
            int tmp = r.nextInt(2);
            int move = tmp == 0 ? 1 : -1;
            doorX += move;
            builder.getBlocks().put(Position3D.create(leftUpper.getX() + doorX, y, 0), new Floor());

            // call recursively
            // top
            binarySpacePartitioning(builder, leftUpper, width, (y - leftUpper.getY()) + 1, depth + 1);
            // bottom
            binarySpacePartitioning(builder, Position3D.create(leftUpper.getX(), y, 0), width, ((leftUpper.getY() + (height - 1)) - y) + 1, depth + 1);
        }
    }

    Area buildLevel() {
        // Start with an empty area
        AreaBuilder areaBuilder = (new EmptyAreaBuilder()).create();

//        // remove outer walls
//        var blocks = areaBuilder.getBlocks();
//        for (int i = 0; i < areaBuilder.getWidth(); i++) {
//            for (int j = 0; j < areaBuilder.getHeight(); j++) {
//                if (i == 0 || j == 0 || i == areaBuilder.getWidth() - 1 || j == areaBuilder.getHeight() - 1)
//                    blocks.put(Position3D.create(i, j, 0), new Floor());
//            }
//        }

        // create rooms using Binary Space Partitioning
        binarySpacePartitioning(areaBuilder, Position3D.create(0, 0, 0), areaBuilder.getWidth(), areaBuilder.getHeight(), 0);

        // Place the player at an empty location in the top-left 1/16
        areaBuilder.addAtEmptyPosition(
                areaBuilder.getPlayer(),
                Position3D.create(1, 1, 0),
                Size3D.create((areaBuilder.getWidth() - 2) / 4, (areaBuilder.getHeight() - 2) / 4, 1)
        );
        // Place the stairs at an empty location in the bottom-right 1/16
        areaBuilder.addAtEmptyPosition(
                new Stairs(),
                Position3D.create((int) (areaBuilder.getWidth() * (3f/4f)), (int) (areaBuilder.getHeight() * (3f/4f)), 0),
                Size3D.create((areaBuilder.getWidth() - 2) / 4, (areaBuilder.getHeight() - 2) / 4, 1)
        );

        // add rats
        for (int i = 0; i <= currentLevel; ++i) {
            areaBuilder.addAtEmptyPosition(new Rat(2 + currentLevel, currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
            areaBuilder.addAtEmptyPosition(new Rat(2 + currentLevel + 1, 1 + currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
        }

        // add snakes
        for (int i = 1; i <= currentLevel; ++i) {
            areaBuilder.addAtEmptyPosition(new Snake(3 + currentLevel, 1 + currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
        }

        // add health pots
        for (int i = 0; i <= currentLevel; i++) {
            areaBuilder.addAtEmptyPosition(new HealthPot(3 + currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
            areaBuilder.addAtEmptyPosition(new HealthPot(5 + currentLevel), Position3D.defaultPosition(), areaBuilder.getSize());
        }

        // Build it into a full Area
        return areaBuilder.build();
    }

    /**
     * Moving down - goes to a brand new level.
     */
    @Override
    public void moveDown() {
        ++currentLevel;
        (new LoggedEvent(this, "Descended to level " + (currentLevel + 1))).emit();
        if (currentLevel >= getAreas().getSize()) getAreas().add(buildLevel());
        goToArea(getAreas().get(currentLevel));
    }

    /**
     * Moving up would be for revisiting past levels, we do not need that. Check [DungeonWorld] for an implementation.
     */
    @Override
    public void moveUp() {
        // Not implemented
    }
}
