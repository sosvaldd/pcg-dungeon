package cz.cuni.gamedev.nail123.roguelike.mechanics;

import cz.cuni.gamedev.nail123.roguelike.entities.Player;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasCombatStats;
import cz.cuni.gamedev.nail123.roguelike.events.LogEventsKt;
import kotlin.random.Random;

import static java.lang.Math.max;

public class Combat {
    public static void attack(HasCombatStats attacker, HasCombatStats defender) {
        int damage;

        if (attacker instanceof Player && attacker.getRNG()) {
            double luck = Random.Default.nextDouble();
            if (luck < 1/3f) {
                damage = 0;
                LogEventsKt.logMessage(attacker, "Out of luck.");
            } else if (luck < 2/3f) {
                damage = max(attacker.getAttack() - defender.getDefense(), 0);
                LogEventsKt.logMessage(attacker, "Better than nothing.");
            } else {
                damage = max(attacker.getAttack() - defender.getDefense(), 0);
                damage *= 2;
                LogEventsKt.logMessage(attacker, "Blessed by RNG god!");
            }
        } else {
            damage = max(attacker.getAttack() - defender.getDefense(), 0);

            if (attacker instanceof Player && Random.Default.nextDouble() < attacker.getCritChance()) {
                damage *= 2;
                LogEventsKt.logMessage(attacker, "Critical hit!");
            }
        }

        defender.takeDamage(damage);

        if (attacker instanceof Player) {
            LogEventsKt.logMessage(attacker, String.format("You hit %s for %d damage!", defender, damage));
        } else if (defender instanceof Player) {
            LogEventsKt.logMessage(attacker, String.format("%s hits you for %d damage!", attacker, damage));
        } else {
            LogEventsKt.logMessage(attacker, String.format("%s hits %s for %d damage!", attacker, defender, damage));
        }
    }
}

