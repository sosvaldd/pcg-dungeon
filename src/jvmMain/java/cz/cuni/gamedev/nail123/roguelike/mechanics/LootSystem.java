package cz.cuni.gamedev.nail123.roguelike.mechanics;

import cz.cuni.gamedev.nail123.roguelike.blocks.GameBlock;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Enemy;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Rat;
import cz.cuni.gamedev.nail123.roguelike.entities.enemies.Snake;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Axe;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Item;
import cz.cuni.gamedev.nail123.roguelike.entities.items.Sword;
import kotlin.Pair;
import kotlin.random.Random;

import java.util.*;
import java.util.function.Supplier;

public class LootSystem {
    interface ItemDrop {
        List<Item> getDrops();
    }
    static class NoDrop implements ItemDrop {
        // Singleton pattern
        static NoDrop instance = null;
        private NoDrop() {}

        public static NoDrop getInstance() {
            if (instance == null) {
                instance = new NoDrop();
            }
            return instance;
        }

        @Override
        public List<Item> getDrops() { return new ArrayList<>(); }
    }
    static class OneDrop implements ItemDrop {
        Supplier<Item> instantatior;
        public OneDrop(Supplier<Item> instantiator) {
            this.instantatior = instantiator;
        }
        @Override
        public List<Item> getDrops() { return Collections.singletonList(instantatior.get()); }
    }
    static class TreasureClass implements ItemDrop {
        int numDrops;
        List<Pair<Integer, ItemDrop>> possibleDrops;
        int totalProb;

        public TreasureClass(int numDrops, List<Pair<Integer, ItemDrop>> possibleDrops) {
            this.numDrops = numDrops;
            this.possibleDrops = possibleDrops;
            for (Pair<Integer, ItemDrop> drop : possibleDrops) {
                totalProb += drop.getFirst();
            }
        }

        @Override
        public List<Item> getDrops() {
            ArrayList<Item> drops = new ArrayList<Item>();
            for (int i = 0; i < numDrops; ++i) {
                drops.addAll(pickDrop().getDrops());
            }
            return drops;
        }

        private ItemDrop pickDrop() {
            int randNumber = Random.Default.nextInt(totalProb);
            for (Pair<Integer, ItemDrop> drop : possibleDrops) {
                randNumber -= drop.getFirst();
                if (randNumber < 0) return drop.getSecond();
            }
            // Never happens, but we need to place something here anyway
            return possibleDrops.get(possibleDrops.size() - 1).getSecond();
        }
    }

    // Store rng for convenience
    static Random rng = Random.Default;

    // Sword with power 2-4, 0% crit chance
    static OneDrop basicSword = new OneDrop(() -> new Sword(rng.nextInt(3) + 2, 0f, false, "Basic "));
    // Sword with power 5-6, 0% crit chance
    static OneDrop strongSword = new OneDrop(() -> new Sword(rng.nextInt(2) + 5, 0f, false, "Strong "));
    // Sword with power 4-5, 50% crit chance
    static OneDrop critSword = new OneDrop(() -> new Sword(rng.nextInt(2) + 4, 0.5f, false, "Crit "));
    // RNG sword, 33% 0 damage, 33% 5 damage, 33% 10 damage
    static OneDrop RNGSword = new OneDrop(() -> new Sword(5, 0f, true, "RNG "));

    // axes - basically stronger versions of swords
    static OneDrop basicAxe = new OneDrop(() -> new Axe(rng.nextInt(3) + 3, 0f, false, "Basic "));
    static OneDrop strongAxe = new OneDrop(() -> new Axe(rng.nextInt(2) + 6, 0f, false, "Strong "));
    static OneDrop critAxe = new OneDrop(() -> new Axe(rng.nextInt(2) + 5, 0.7f, false, "Crit "));
    static OneDrop RNGAxe = new OneDrop(() -> new Axe(7, 0f, true, "RNG "));

    @SuppressWarnings("rawtypes")
    static Map<Class, ItemDrop> enemyDrops = new HashMap<>();

    static {
        enemyDrops.put(Rat.class, new TreasureClass(1, Arrays.asList(
                new Pair<>(30, NoDrop.getInstance()),
                new Pair<>(30, basicSword),
                new Pair<>(10, strongSword),
                new Pair<>(10, critSword),
                new Pair<>(10, RNGSword)
        )));
        enemyDrops.put(Snake.class, new TreasureClass(2, Arrays.asList(
                new Pair<>(30, NoDrop.getInstance()),
                new Pair<>(30, basicAxe),
                new Pair<>(10, strongAxe),
                new Pair<>(10, critAxe),
                new Pair<>(10, RNGAxe)
        )));
    }

    public static void onDeath(Enemy enemy) {
        ItemDrop drop = enemyDrops.get(enemy.getClass());
        if (drop == null) return;
        for (Item item : drop.getDrops()) {
            GameBlock block = enemy.getArea().get(enemy.getPosition());
            if (block != null) {
                block.getEntities().add(item);
            }
        }
    }
}
