package cz.cuni.gamedev.nail123.roguelike.entities.items;

import cz.cuni.gamedev.nail123.roguelike.entities.Player;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.Inventory;
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles;
import org.jetbrains.annotations.NotNull;

import static cz.cuni.gamedev.nail123.roguelike.events.LogEventsKt.logMessage;

public class HealthPot extends Item {
    private int healPower;
    private boolean firstEquipCall = true;

    public HealthPot(int healPower) {
        super(GameTiles.INSTANCE.getHEALTH_POT());
        this.healPower = healPower;
    }

    @NotNull
    @Override
    public Inventory.EquipResult isEquipable(@NotNull HasInventory character) {
        return new Inventory.EquipResult(true, "");
    }

    @Override
    protected void onEquip(@NotNull HasInventory character) {
        if (firstEquipCall) {
            firstEquipCall = false;
            logMessage(this, "Picked up Heal Potion(" + healPower + ")");
            return;
        }
        if (character instanceof Player) {
            int healthRestored = Math.min(healPower, ((Player) character).getMaxHitpoints() - ((Player) character).getHitpoints());
            ((Player) character).setHitpoints(((Player) character).getHitpoints() + healthRestored);
            character.getInventory().remove(this);
            logMessage(this, "You drank a Heal Potion, which restored " + healthRestored + " health points!");
        }
    }

    @Override
    protected void onUnequip(@NotNull HasInventory character) { }

    @NotNull
    @Override
    public String toString() {
        return "Heal Potion(" + healPower + ")";
    }
}
