package cz.cuni.gamedev.nail123.roguelike.entities.items;

import cz.cuni.gamedev.nail123.roguelike.entities.Player;
import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasInventory;
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles;
import org.jetbrains.annotations.NotNull;

public class Axe extends Weapon {
    private int attackPower;
    private float critChange;
    private boolean RNG;
    private String prefix;

    public Axe(int attackPower, float critChance, boolean RNG, String prefix) {
        super(GameTiles.INSTANCE.getAXE());
        this.attackPower = attackPower;
        this.critChange = critChance;
        this.RNG = RNG;
        this.prefix = prefix;
    }

    @Override
    protected void onEquip(@NotNull HasInventory character) {
        if (character instanceof Player) {
            ((Player) character).setAttack(((Player) character).getAttack() + attackPower);
            ((Player) character).setCritChance(((Player) character).getCritChance() + critChange);
            ((Player) character).setRNG(RNG);
        }
    }

    @Override
    protected void onUnequip(@NotNull HasInventory character) {
        if (character instanceof Player) {
            ((Player) character).setAttack(((Player) character).getAttack() - attackPower);
            ((Player) character).setCritChance(((Player) character).getCritChance() - critChange);
            ((Player) character).setRNG(false);
        }
    }

    @NotNull
    @Override
    public String toString() {
        return prefix + "Axe(" + attackPower + ")";
    }
}
