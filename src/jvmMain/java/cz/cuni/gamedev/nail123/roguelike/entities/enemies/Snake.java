package cz.cuni.gamedev.nail123.roguelike.entities.enemies;

import cz.cuni.gamedev.nail123.roguelike.entities.attributes.HasVision;
import cz.cuni.gamedev.nail123.roguelike.mechanics.NavigationKt;
import cz.cuni.gamedev.nail123.roguelike.mechanics.Vision;
import cz.cuni.gamedev.nail123.roguelike.tiles.GameTiles;
import org.hexworks.zircon.api.data.Position3D;

import java.util.List;

public class Snake extends Enemy implements HasVision {
    protected int hitpoints = getMaxHitpoints();
    protected int attack;
    protected int defense;

    public Snake(int attack, int defense) {
        super(GameTiles.INSTANCE.getSNAKE());
        this.attack = attack;
        this.defense = defense;
    }

    @Override public boolean getBlocksVision() { return false; }
    @Override public int getMaxHitpoints() { return 15; }
    @Override public int getHitpoints() { return this.hitpoints; }
    @Override public void setHitpoints(int hitpoints) { this.hitpoints = hitpoints; }
    @Override public int getAttack() { return attack; }
    @Override public void setAttack(int attack) { this.attack = attack; }
    @Override public int getDefense() { return this.defense; }
    @Override public void setDefense(int defense) { this.defense = defense; }
    @Override public int getVisionRadius() { return 10; }
    @Override public float getCritChance() { return 0f; }
    @Override public void setCritChance(float critChance) { }
    @Override public boolean getRNG() { return false; }
    @Override public void setRNG(boolean RNG) { }

    protected boolean isChasingPlayer = false;
    protected int patience = 3;
    protected boolean hasJustMoved = false;
    @Override
    public void update() {
        // Get the player position
        Position3D playerPosition = getArea().getPlayer().getPosition();
        // Use the Vision mechanic to determine if this orc can see player
        List<Position3D> visible = Vision.INSTANCE.getVisiblePositionsFrom(getArea(), getPosition(), getVisionRadius());
        boolean canSeePlayer = visible.contains(playerPosition);

        if (canSeePlayer) {
            isChasingPlayer = true;
            patience = 3;
            if (!hasJustMoved) {
                NavigationKt.goSmartlyTowards(this, playerPosition);
                hasJustMoved = true;
            }
            else {
                hasJustMoved = false;
            }
        } else {
            if (isChasingPlayer && patience > 3) {
                patience -= 1;
                if (!hasJustMoved) {
                    NavigationKt.goSmartlyTowards(this, playerPosition);
                    hasJustMoved = true;
                }
                else {
                    hasJustMoved = false;
                }
            } else if (isChasingPlayer && patience == 0) {
                isChasingPlayer = false;
                hasJustMoved = false;
            } else {
                hasJustMoved = false;
            }
        }
    }
}
